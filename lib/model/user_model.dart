class UserModel{
  late int _UserID;
  late String _Name;
  late String _DOB;
  late int _CityID;
  late bool _isFavouriteUser;

  bool get isFavouriteUser => _isFavouriteUser;

  set isFavouriteUser(bool isFavouriteUser) {
    _isFavouriteUser = isFavouriteUser;
  }

  int get UserID => _UserID;

  set UserID(int UserID) {
    _UserID = UserID;
  }

  String get Name => _Name;

  set Name(String Name) {
    _Name = Name;
  }

  String get DOB => _DOB;

  set DOB(String DOB) {
    _DOB = DOB;
  }

  int get CityID => _CityID;

  set CityID(int CityID) {
    _CityID = CityID;
  }
}