import 'dart:io';

import 'package:database_demo/database/database.dart';
import 'package:database_demo/model/city_model.dart';
import 'package:database_demo/model/user_model.dart';
import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:database_demo/database/database.dart';
import 'package:database_demo/model/city_model.dart';
import 'package:database_demo/model/user_model.dart';
import 'package:intl/intl.dart';

class AddUser extends StatefulWidget {
  late UserModel? model;

  AddUser({required this.model});

  @override
  State<AddUser> createState() => _AddUserState();
}

class _AddUserState extends State<AddUser> {
  CityModel model =
  CityModel(CityID1: -1, CityName1: 'Select City', StateID1: -1);
  bool isGetCity = true;
  DateTime selectedDate = DateTime.now();
  final _formKey = GlobalKey<FormState>();
  late TextEditingController nameController;
  MyDatabase myDatabase = MyDatabase();

  @override
  void initState() {
    super.initState();
    nameController = TextEditingController(
        text: widget.model != null ? widget.model!.Name.toString() : '');
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Add New User'),
        ),
        body: Form(
          key: _formKey,
          child: Column(
            children: [
              Container(
                width: double.infinity,
                child: FutureBuilder<List<CityModel>>(
                  builder: (context, snapshot) {
                    if (snapshot.hasData && snapshot.data != null) {
                      if (isGetCity) {
                        model = snapshot.data![0];
                      }
                      return DropdownButtonHideUnderline(
                        child: DropdownButton2(
                          items: snapshot.data!
                              .map((item) => DropdownMenuItem<CityModel>(
                                    value: item,
                                    child: Text(
                                      item.CityName.toString(),
                                      style: const TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                      ),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ))
                              .toList(),
                          value: model,
                          onChanged: (value) {
                            setState(() {
                              isGetCity = false;
                              model = value!;
                            });
                          },
                          icon: const Icon(
                            Icons.arrow_drop_down_outlined,
                          ),
                          buttonDecoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(14),
                            border: Border.all(
                              color: Colors.black26,
                            ),
                          ),
                          dropdownDecoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(14),
                          ),
                        ),
                      );
                    } else {
                      return Container();
                    }
                  },
                  future: isGetCity ? myDatabase.getCityListFromTbl() : null,
                ),
              ),
              TextFormField(
                controller: nameController,
                validator: (value) {
                  if (value == null || value.trim().length == 0) {
                    return 'Enter Valid Name';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  hintText: 'Enter Name',
                ),
              ),
              SizedBox(
                height: 5,
              ),
              InkWell(
                child: Text(getFormattedDateTime(selectedDate)),
                onTap: _pickDateDialog,
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                child: TextButton(
                    onPressed: () {
                      setState(() async {
                        if (_formKey.currentState!.validate()) {
                          if (model.CityID == -1) {
                            showAlertDialog(context);
                          }
                        } else {
                           await myDatabase.upsertIntoUserTable(
                               cityId: model.CityID,
                               userName: nameController.text.toString(),
                               dob: selectedDate.toString(),
                               userID: widget.model != null
                                   ? widget.model!.UserID
                                   : -1);
                        }
                      });
                    },
                    child: Text(
                      'Submit',
                      style: TextStyle(color: Colors.white),
                    )),
                color: Colors.blue,
              ),
            ],
          ),
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {},
    );

    AlertDialog alert = AlertDialog(
      title: Text("Alert"),
      content: Text("Please select city"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void _pickDateDialog() {
    showDatePicker(
            context: context,
            initialDate: selectedDate,
            firstDate: DateTime(1950),
            lastDate: DateTime.now())
        .then((pickedDate) {
      if (pickedDate == null) {
        return;
      }
      setState(() {
        selectedDate = pickedDate;
      });
    });
  }


  String getFormattedDateTime(dateToFormat) {
    if (dateToFormat != null) {
      return DateFormat('dd-MM-yyyy').format(dateToFormat).toString();
    } else {
      return DateFormat('dd-MM-yyyy').format(DateTime.now()).toString();
    }
  }
}
