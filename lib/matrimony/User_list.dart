//region IMPORTS
import 'package:database_demo/matrimony/add_user.dart';
import 'package:flutter/material.dart';
import 'package:database_demo/database/database.dart';
import 'package:database_demo/model/user_model.dart';

//endregion

//region MAIN CLASS
class UserListPage extends StatefulWidget {
  @override
  State<UserListPage> createState() => _UserListPageState();
}
//endregion

//region STATE CLASS
class _UserListPageState extends State<UserListPage> {

  //region VARIABLE DECLARATION
  MyDatabase db = MyDatabase();
  List<UserModel> localList = [];
  List<UserModel> searchList = [];
  bool isGetData = true;
  TextEditingController controller = TextEditingController();
  //endregion

  //region INIT STATE
  @override
  void initState() {
    super.initState();
  }
  //endregion

  //region BUILD METHOD
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          InkWell(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => AddUser(model: null),
                ),
              );
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Icon(
                    size: 24,
                    Icons.add,
                    color: Colors.white,
                  ),
                  Text('Add'),
                ],
              ),
            ),
          )
        ],
        title: Row(
          children: [
            Icon(
              Icons.person,
              color: Colors.white,
            ),
            SizedBox(
              width: 5,
            ),
            Text('User List'),
          ],
        ),
      ),
      body: Container(
        color: Colors.white,
        child: FutureBuilder<List<UserModel>>(
          builder: (context, snapshot) {
            if (snapshot != null && snapshot.hasData) {
              if (isGetData) {
                localList.addAll(snapshot.data!);
                searchList.addAll(localList);
              }
              isGetData = false;
              return Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  TextField(
                    controller: controller,
                    onChanged: (value) {
                      searchList.clear();
                      for (int i = 0; i < localList.length; i++) {
                        if (localList[i]
                            .Name
                            .toLowerCase()
                            .contains(value.toLowerCase())) {
                          searchList.add(localList[i]);
                        }
                      }
                      setState(() {});
                    },
                    decoration: InputDecoration(hintText: 'Search User'),
                  ),
                  Expanded(
                    child: ListView.builder(
                      padding: EdgeInsets.all(5),
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) =>
                                  AddUser(model: searchList![index]),
                            ));
                          },
                          child: Card(
                            color: Colors.grey.shade100,
                            elevation: 5.0,
                            borderOnForeground: true,
                            child: Container(
                              padding: EdgeInsets.all(5),
                              child: Row(
                                children: [
                                  InkWell(
                                    child: Icon(
                                      Icons.delete,
                                      color: Colors.red,
                                      size: 24,
                                    ),
                                    onTap: () {
                                      showAlertDialog(context, index);
                                    },
                                  ),
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(left: 10),
                                      child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            (searchList![index]
                                                .Name
                                                .toString()),
                                          ),
                                          Text(
                                            searchList![index].DOB.toString(),
                                            style: TextStyle(
                                                color: Colors.grey.shade500,
                                                fontSize: 12),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () {
                                      setState(() {

                                        searchList[index].isFavouriteUser =
                                        !searchList[index].isFavouriteUser;
                                      });
                                    },
                                    child: Icon(
                                        searchList[index].isFavouriteUser
                                            ? Icons.favorite
                                            : Icons.favorite_border,
                                        color: Colors.red.shade400,
                                        size: 24),
                                  ),
                                  Icon(Icons.keyboard_arrow_right,
                                      color: Colors.grey.shade400, size: 15),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                      itemCount: searchList!.length,
                    ),
                  ),
                ],
              );
            } else {
              return Center(
                child: Text('No User Found'),
              );
            }
          },
          future: isGetData ? db.getUserListFromTbl() : null,
        ),
      ),
    );
  }
  //endregion

  //region ALERT DIALOG FOR DELETE
  showAlertDialog(BuildContext context, index) {
    Widget yesButton = TextButton(
      child: Text("Yes"),
      onPressed: () async {
        int deletedUserID =
        await db.deleteUserFromUserTable(localList[index].UserID);
        if (deletedUserID > 0) {
          localList.removeAt(index);
        }
        Navigator.pop(context);
        setState(() {});
      },
    );
    Widget noButton = TextButton(
      child: Text("No"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text("Alert"),
      content: Text("Are you sure want to delete?"),
      actions: [
        yesButton,
        noButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
//endregion
}
//endregion
