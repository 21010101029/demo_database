import 'package:intl/intl.dart';

class Utils{
  static String getFormattedDateTime(dateToFormat) {
    if (dateToFormat != null) {
      return DateFormat('dd-MM-yyyy').format(dateToFormat).toString();
    } else {
      return DateFormat('dd-MM-yyyy').format(DateTime.now()).toString();
    }
  }
}
